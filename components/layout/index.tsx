import { FC, ReactNode, useLayoutEffect, useRef, useState } from 'react';
import { ResizableBox } from 'react-resizable';

interface LayoutProps {
  children: ReactNode;
}

const Layout: FC<LayoutProps> = (props) => {
  const [{ mainWidth, sideWidth }, setWidth] = useState({
    mainWidth: 0,
    sideWidth: 400,
    width: 0,
  });
  const appElemRef = useRef<HTMLDivElement | null>(null);

  useLayoutEffect(() => {
    const { width } = appElemRef.current?.getBoundingClientRect()!;
    setWidth((pre) => ({
      width,
      sideWidth: pre.sideWidth,
      mainWidth: width - pre.sideWidth,
    }));
  }, []);

  return (
    <div className="app" ref={appElemRef}>
      <ResizableBox
        className="main"
        axis="x"
        width={mainWidth}
        handle={<span className="custom-handle" />}
        resizeHandles={['e']}
        handleSize={[8, 8]}
        onResize={(_, data) => {
          const { size } = data;
          setWidth((pre) => ({
            width: pre.width,
            mainWidth: size.width,
            sideWidth: pre.width - size.width,
          }));
        }}
      >
        <main>main</main>
      </ResizableBox>
      <ResizableBox className="side" width={sideWidth} axis="x">
        <aside>aside</aside>
      </ResizableBox>
    </div>
  );
};

export default Layout;
