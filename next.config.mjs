/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  output: 'export',
  images: {
    loader: 'akamai',
    path: ''
  },
  async redirects() {
    return [
      {
        source: '/api/:path*',
        destination: 'http://localhost:9000/api/:path*',
        permanent: false
      },
    ]
  },
};

export default nextConfig;
